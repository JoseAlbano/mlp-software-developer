# My Learning Path: Java Software Developer

## Book: *Java How to Program (11th Edition)* by *Paul Deitel and Harvey Deitel*

Hello.

Here are my Solutions to the Exercises in the Book.

### My Progress

- [x] Chapter 01: Introduction to Computers, the Internet and Java
- [x] Chapter 02: Introduction to Java Applications; Input/Output and Operators
- [x] Chapter 03: Introduction to Classes, Objects, Methods and Strings
- [ ] Chapter 04: Control Statements: Part 1
- [ ] Chapter 05: Control Statements: Part 2; Logical Operators
- [ ] Chapter 06: Methods: A Deeper Look
- [ ] Chapter 07: Arrays and ArrayLists
- [ ] Chapter 08: Classes and Objects: A Deeper Look
- [ ] Chapter 09: Object-Oriented Programming: Inheritance
- [ ] Chapter 10: Object-Oriented Programming: Polymorphism and Interfaces
- [ ] Chapter 11: Exception Handling: A Deeper Look
- [ ] Chapter 12: JavaFX Graphical User Interfaces: Part 1
- [ ] Chapter 13: JavaFX GUI: Part 2
- [ ] Chapter 14: Strings, Characters and Regular Expressions
- [ ] Chapter 15: Files, Input/Output Streams, NIO and XML Serialization
- [ ] Chapter 16: Generic Collections
- [ ] Chapter 17: Lambdas and Streams
- [ ] Chapter 18: Recursion
- [ ] Chapter 19: Searching, Sorting and Big O
- [ ] Chapter 20: Generic Classes and Methods: A Deeper Look
- [ ] Chapter 21: Custom Generic Data Structures
- [ ] Chapter 22: JavaFX Graphics and Multimedia
- [ ] Chapter 23: Concurrency
- [ ] Chapter 24: Accessing Databases with JDBC
- [ ] Chapter 25: Introduction to JShell: Java 9's REPL
