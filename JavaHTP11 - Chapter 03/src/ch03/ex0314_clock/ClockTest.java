package ch03.ex0314_clock;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  Exercise 3.14 Clock Class
*/

/**
 * @author José Albano
 */
public class ClockTest {
  public static void main(String[] args) {
    System.out.println("*** Exercise 3.14 Clock Class ***\n");

    Clock clock1 = new Clock(7, 30, 0);
    Clock clock2 = new Clock(23, 45, 33);

    System.out.printf("\tClock 1: ");
    clock1.displayTime();

    System.out.printf("\n\tClock 2: ");
    clock2.displayTime();

    System.out.println("\n\n\tSetting Clock 1 Minutes to 45... ");
    clock1.setMinute(45);

    System.out.printf("\tClock 1: ");
    clock1.displayTime();


    System.out.println("\n\n\tSetting Clock 2 Hours to 25... ");
    clock2.setHour(25);

    System.out.printf("\tClock 2: ");
    clock2.displayTime();

    System.out.println();
  }
}