package ch03.ex0314_clock;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  Exercise 3.14 Clock Class
*/

/**
 * @author José Albano
 */
public class Clock {
  private int hour;
  private int minute;
  private int second;

  public Clock(int hour, int minute, int second) {
    this.hour = hour;
    this.minute = minute;
    this.second = second;
  }

  public void displayTime() {
    System.out.printf("%2d:%2d:%2d", hour, minute, second);
  } // displayTime

  public void setHour(int hour) {
    this.hour = 0;
    
    if (hour <= 23) {
      this.hour = hour;
    }
  }

  public int getHour() {
    return hour;
  }

  public void setMinute(int minute) {
    this.minute = 0;
    
    if (minute <= 59) {
      this.minute = minute;
    }
  }

  public int getMinute() {
    return minute;
  }

  public void setSecond(int second) {
    this.second = 0;
    
    if (hour <= 23) {
      this.second = second;
    }
  }

  public int getSecond() {
    return second;
  }
}