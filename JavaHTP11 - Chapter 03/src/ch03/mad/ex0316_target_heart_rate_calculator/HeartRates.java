package ch03.mad.ex0316_target_heart_rate_calculator;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  "Making a Difference" Exercise

  Exercise 3.16 Target-Heart-Rate Calculator
*/

/**
 * @author José Albano
 */
public class HeartRates {
  private String firstName;
  private String lastName;

  private int birthYear;
  private int birthMonth;
  private int birthDay;

  public HeartRates(String firstName, String lastName, int birthYear, 
    int birthMonth, int birthDay) {

    this.firstName = firstName;
    this.lastName = lastName;

    this.birthYear = birthYear;
    this.birthMonth = birthMonth;
    this.birthDay = birthDay;
  } // constructor


  public int getAge() {
    return 2023 - getBirthYear();
  } // getAge

  public int getMaximumHeartRate() {
    return 220 - getAge();
  } // getMaximumHeartRate

  public String getTargetHeartRate() {
    return String.valueOf( getMaximumHeartRate() * .5 ) + "-" + 
      String.valueOf( getMaximumHeartRate() * .85);
  } // getTargetHeartRate

  // getters and setters
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setBirthYear(int birthYear) {
    this.birthYear = birthYear;
  }

  public int getBirthYear() {
    return birthYear;
  }

  public void setBirthMonth(int birthMonth) {
    this.birthMonth = birthMonth;
  }

  public int getBirthMonth() {
    return birthMonth;
  }

  public void setBirthDay(int birthDay) {
    this.birthDay = birthDay;
  }

  public int getBirthDay() {
    return birthDay;
  }
}