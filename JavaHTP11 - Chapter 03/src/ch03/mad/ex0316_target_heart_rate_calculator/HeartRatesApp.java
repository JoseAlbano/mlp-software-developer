package ch03.mad.ex0316_target_heart_rate_calculator;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  "Making a Difference" Exercise

  Exercise 3.16 Target-Heart-Rate Calculator
*/
import java.util.Scanner;

/**
 * @author José Albano
 */
public class HeartRatesApp {
  public static void main(String[] args) {
    System.out.println("*** Exercise 3.16 Target-Heart-Rate Calculator ***\n");

    HeartRates myHeartRates;

    Scanner input = new Scanner(System.in);
    
    String firstName;
    String lastName;
    int birthYear;
    int birthMonth;
    int birthDay;

    
    System.out.print("\tEnter the First Name: ");
    firstName = input.nextLine();

    System.out.print("\tEnter the Last Name: ");
    lastName = input.nextLine();

    System.out.print("\tEnter the Birth Year: ");
    birthYear = input.nextInt();

    System.out.print("\tEnter the Birth Month: ");
    birthMonth = input.nextInt();

    System.out.print("\tEnter the Birth Day: ");
    birthDay = input.nextInt();

    myHeartRates = new HeartRates(firstName, lastName, birthYear, birthMonth, 
      birthDay);

    System.out.printf("\n\t\t%s, %s\n\t\tBirthdate: %d/%d/%d\n", 
      myHeartRates.getLastName(), myHeartRates.getFirstName(), 
      myHeartRates.getBirthYear(), myHeartRates.getBirthMonth(),
      myHeartRates.getBirthDay() );

    System.out.printf("\n\t\tAge: %d\n\t\tMaximum Heart Rate: %d" +
      "\n\t\tTarget-Heart-Rate range: %s\n\n", myHeartRates.getAge(),
      myHeartRates.getMaximumHeartRate(), myHeartRates.getTargetHeartRate() );
  }
}