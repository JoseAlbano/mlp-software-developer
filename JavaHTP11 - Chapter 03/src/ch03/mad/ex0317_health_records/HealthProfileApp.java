package ch03.mad.ex0317_health_records;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  "Making a Difference" Exercise

  Exercise 3.17 Computerization of Health Records
*/
import java.util.Scanner;

public class HealthProfileApp {
  public static void main(String[] args) {
    System.out.println("*** Exercise 3.17 Computerization of Health Records ***\n");

    HealthProfile myHealthProfile;

    Scanner input = new Scanner(System.in);
    
    String firstName;
    String lastName;
    String gender;
    int birthYear;
    int birthMonth;
    int birthDay;
    double heightInInches;
    double weightInPounds;

    
    System.out.print("\tEnter the First Name: ");
    firstName = input.nextLine();

    System.out.print("\tEnter the Last Name: ");
    lastName = input.nextLine();

    System.out.print("\tEnter the Gender: ");
    gender = input.nextLine();

    System.out.print("\tEnter the Birth Year: ");
    birthYear = input.nextInt();

    System.out.print("\tEnter the Birth Month: ");
    birthMonth = input.nextInt();

    System.out.print("\tEnter the Birth Day: ");
    birthDay = input.nextInt();

    System.out.print("\tEnter the Height (in Inches): ");
    heightInInches = input.nextDouble();

    System.out.print("\tEnter the Weight (in Pounds): ");
    weightInPounds = input.nextDouble();


    myHealthProfile = new HealthProfile(firstName, lastName, gender, birthYear,
      birthMonth, birthDay, heightInInches, weightInPounds);
    

    System.out.printf("\n\t\t%s, %s\n\n\t\tBirthdate: %d/%d/%d\n\t\tGender: %s"
      + "\n\t\tHeight (in Inches): %.2f\n\t\tWeight (in Pounds): %.2f", 
      myHealthProfile.getLastName(), myHealthProfile.getFirstName(),
      myHealthProfile.getBirthYear(), myHealthProfile.getBirthMonth(), 
      myHealthProfile.getBirthDay(), myHealthProfile.getGender(), 
      myHealthProfile.getHeightInInches(), myHealthProfile.getWeightInPounds() 
    );

    System.out.printf("\n\n\t\tAge: %d\n\n\t\tBMI: %.2f", 
      myHealthProfile.getAge(), myHealthProfile.bmiInPounds()
    );

    System.out.println("\n\n\t\tBMI Categories:\n" +
      "\n\t\t\tUnderweight = <18.5\n\t\t\tNormal weight = 18.5–24.9" + 
      "\n\t\t\tOverweight = 25–29.9\n\t\t\tObesity = BMI of 30 or greater");

    System.out.printf("\n\t\tMaximum Heart Rate: %d" +
      "\n\t\tTarget-Heart-Rate range: %s\n\n", 
      myHealthProfile.getMaximumHeartRate(), 
      myHealthProfile.getTargetHeartRate());
  }
}