package ch03.mad.ex0317_health_records;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  "Making a Difference" Exercise

  Exercise 3.17 Computerization of Health Records
*/
public class HealthProfile {
  private String firstName;
  private String lastName;
  private String gender;

  private int birthYear;
  private int birthMonth;
  private int birthDay;

  private double heightInInches;
  private double weightInPounds;
  private double heightInMeters;
  private double weightInKilograms;

  public HealthProfile(String firstName, String lastName, String gender, 
    int birthYear, int birthMonth, int birthDay, 
    double heightInInches,
    double weightInPounds) {

    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;

    this.birthYear = birthYear;
    this.birthMonth = birthMonth;
    this.birthDay = birthDay;

    this.heightInInches = heightInInches;
    this.weightInPounds = weightInPounds;
  } // constructor


  public int getAge() {
    return 2023 - getBirthYear();
  } // getAge

  public int getMaximumHeartRate() {
    return 220 - getAge();
  } // getMaximumHeartRate

  public String getTargetHeartRate() {
    return String.valueOf( getMaximumHeartRate() * .5 ) + "-" + 
      String.valueOf( getMaximumHeartRate() * .85);
  } // getTargetHeartRate

  public double bmiInPounds() {
    return (weightInPounds * 703) / (heightInInches * heightInInches);
  } // bmiInPounds

  public double bmiInKilograms() {
    return weightInKilograms / (heightInMeters * heightInMeters);
  } // bmiInKilograms

  // getters and setters
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setBirthYear(int birthYear) {
    this.birthYear = birthYear;
  }

  public int getBirthYear() {
    return birthYear;
  }

  public void setBirthMonth(int birthMonth) {
    this.birthMonth = birthMonth;
  }

  public int getBirthMonth() {
    return birthMonth;
  }

  public void setBirthDay(int birthDay) {
    this.birthDay = birthDay;
  }

  public int getBirthDay() {
    return birthDay;
  }

  public void setHeightInInches(double heightInInches) {
    this.heightInInches = heightInInches;
  }

  public double getHeightInInches() {
    return heightInInches;
  }

  public void setWeightInPounds(double weightInPounds) {
    this.weightInPounds = weightInPounds;
  }

  public double getWeightInPounds() {
    return weightInPounds;
  }
} // class