package ch03.ex0312_petrol_purchase;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings
  Exercise 3.12 Class PetrolPurchase
*/

import java.util.Scanner;

/**
 * @author José Albano
 */
public class PetrolApp {
  public static void main(String[] args) {
    System.out.println("*** Exercise 3.12 Class PetrolPurchase ***\n");

    PetrolPurchase myPetrolPurchase;

    String stationLocation;
    String petrolType;
    int quantityInLiters;
    double pricePerLiter;
    double percentageDiscount;

    // Create a Scanner to obtain input from the command window
    Scanner input = new Scanner(System.in);
    
    System.out.print("Enter the Station Location: ");
    stationLocation = input.nextLine();

    System.out.print("Enter the Petrol Type: ");
    petrolType = input.nextLine();

    System.out.print("Enter the Quantity in Liters: ");
    quantityInLiters = input.nextInt();

    System.out.print("Enter the Price per Liter: ");
    pricePerLiter = input.nextDouble();

    System.out.print("Enter the Percentage Discount (%): ");
    percentageDiscount = input.nextDouble();

    myPetrolPurchase = new PetrolPurchase(stationLocation, petrolType, 
      quantityInLiters, pricePerLiter, percentageDiscount);

    System.out.printf("\n\tThe Petrol Purchase is $%.2f\n\n", 
      myPetrolPurchase.getPurchaseAmount());
  }
}