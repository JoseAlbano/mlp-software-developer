package ch03.ex0312_petrol_purchase;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  Exercise 3.12 Class PetrolPurchase
*/

/**
 * @author José Albano
 */
public class PetrolPurchase {

  private String stationLocation;
  private String petrolType;
  private int quantityInLiters;
  private double pricePerLiter;
  private double percentageDiscount;

  public PetrolPurchase(String stationLocation, String petrolType, 
    int quantityInLiters, double pricePerLiter, double percentageDiscount) {
    
    this.stationLocation = stationLocation;
    this.petrolType = petrolType;
    this.quantityInLiters = quantityInLiters;
    this.pricePerLiter = pricePerLiter;
    this.percentageDiscount = percentageDiscount;
  } // constructor

  public double getPurchaseAmount() {
    double purchaseAmount = getQuantityInLiters() * getPricePerLiter();
    
    return purchaseAmount - (getPercentageDiscount() / 100 * purchaseAmount );
  } // getPurchaseAmount

  // getters and setters
  public String getStationLocation() {
    return stationLocation;
  }
  
  public void setStationLocation(String stationLocation) {
    this.stationLocation = stationLocation;
  }

  public String getPetrolType() {
    return petrolType;
  }

  public void setPetrolType(String petrolType) {
    this.petrolType = petrolType;
  }

  public int getQuantityInLiters() {
    return quantityInLiters;
  }

  public void setQuantityInLiters(int quantityInLiters) {
    if (quantityInLiters > 0) {
      this.quantityInLiters = quantityInLiters;
    }    
  }

  public double getPricePerLiter() {
    return pricePerLiter;
  }

  public void setPricePerLiter(double pricePerLiter) {
    if (pricePerLiter > 0.0) {
      this.pricePerLiter = pricePerLiter;
    }
  }

  public double getPercentageDiscount() {
    return percentageDiscount;
  }

  public void setPercentageDiscount(double percentageDiscount) {
    if (percentageDiscount > 0.0) {
      this.percentageDiscount = percentageDiscount;
    }
  }
} // class