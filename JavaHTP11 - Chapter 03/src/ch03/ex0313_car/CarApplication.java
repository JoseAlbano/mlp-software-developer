package ch03.ex0313_car;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  Exercise 3.13 Car Class
*/

/**
 * @author José Albano
 */
public class CarApplication {
  public static void main(String[] args) {
    System.out.println("*** Exercise 3.13 Car Class ***\n");

    Car car1 = new Car("Renault Logan", "2023", 5900000);
    Car car2 = new Car("Renault Sandero", "2023", 6400000);

    System.out.printf("\t%s, %s:\t$ARS %.2f\n", 
      car1.getModel(), car1.getYear(), car1.getPrice());

    System.out.printf("\t%s, %s:\t$ARS %.2f\n", 
      car2.getModel(), car2.getYear(), car2.getPrice());


    car1.setPrice( car1.getPrice() - car1.getPrice() * .05 );
    car2.setPrice( car2.getPrice() - car2.getPrice() * .07 );
    

    System.out.println("\n\tCar Prices after the Discount:\n");

    System.out.printf("\t%s, %s:\t$ARS %.2f\n", 
      car1.getModel(), car1.getYear(), car1.getPrice());

    System.out.printf("\t%s, %s:\t$ARS %.2f\n", 
      car2.getModel(), car2.getYear(), car2.getPrice());
  }
}