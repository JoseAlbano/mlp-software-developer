package ch03.ex0313_car;

/* Chapter 3: Introduction to Classes, Objects, Methods and Strings

  Exercise 3.13 Car Class
*/

/**
 * @author José Albano
 */
public class Car {
  private String model;
  private String year;
  private double price;

  public Car (String model, String year, double price) {
    this.model = model;
    this.year = year;
    this.price = price;
  } // constructor

  // Getters and Setters
  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    if (price > 0.0) {
      this.price = price;
    }
  }
}