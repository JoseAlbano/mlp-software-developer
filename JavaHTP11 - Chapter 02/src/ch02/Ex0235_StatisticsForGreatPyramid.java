package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Making a Difference exercise
// Exercise 2.35 Statistics for the Great Pyramid of Giza

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0235_StatisticsForGreatPyramid {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.35 Statistics for the Great Pyramid of Giza ***\n");

    Scanner input = new Scanner(System.in);

    int estimatedStones;    // 2.300.000
    double stoneAverageWeight; // 2.5 tn
    int yearsToBuild;       // 27

    double weightPerYear;
    double weightPerHour;
    double weightPerMinute;

    double totalWeight;
    int hoursPerYear;
    int minutesPerYear;
    

    System.out.println("Estimated number of stones: ");
    estimatedStones = input.nextInt();

    System.out.println("\nAverage weight of each stone: ");
    stoneAverageWeight = input.nextDouble();

    System.out.println("\nNumber of years taken to build: ");
    yearsToBuild = input.nextInt();

    totalWeight = estimatedStones * stoneAverageWeight;
    hoursPerYear = 365 * 24;
    minutesPerYear = hoursPerYear * 60;

    weightPerYear = totalWeight / yearsToBuild;
    weightPerHour = totalWeight / (hoursPerYear * yearsToBuild );
    weightPerMinute = totalWeight / (minutesPerYear * yearsToBuild );


    System.out.printf("\n\tEstimated weight built per year: %f", weightPerYear );
    System.out.printf("\n\tEstimated weight built per hour: %f", weightPerHour);
    System.out.printf("\n\tEstimated weight built per minute: %f", weightPerMinute );

    System.out.println();
  }
}