package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.30 Separating the Digits in an Integer

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0230_PickoffDigits {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.30 Separating the Digits in an Integer ***\n");

    Scanner input = new Scanner(System.in);

    int number;

    System.out.println("Enter the an Integer Number of five digits: ");
    number = input.nextInt();
    
    System.out.printf("\n\t%d   %d   %d   %d   %d\n", 
      number % 100000 / 10000, 
      number % 10000 / 1000, 
      number % 1000 / 100, 
      number % 100 / 10, 
      number % 10);
  }
}
