package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.28 Diameter, Circumference and Area of a Circle

import java.util.Scanner;

/**
 * @author José Albano
 */
public class Ex0228_Circle {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.28 Diameter, Circumference and Area of a Circle ***\n");

    Scanner input = new Scanner(System.in);

    int radius;
    
    System.out.println("Enter the Radius of the Circle: ");
    radius = input.nextInt();

    System.out.printf("\n\tDiameter: %f\n\tCircumference: %f\n\tArea: %f\n", 
      2.0 * radius, 2 * Math.PI * radius, Math.PI * (radius * radius) );
  }
}