package ch02;
/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.27 Checkerboard Pattern of Asterisks

/**
 * @author José Albano
 */
public class Ex0227_CheckboardPattern {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.27 Checkerboard Pattern of Asterisks ***\n");

    System.out.println("* * * * * * * *");
    System.out.println(" * * * * * * * *");
    System.out.println("* * * * * * * *");
    System.out.println(" * * * * * * * *");
    System.out.println("* * * * * * * *");
    System.out.println(" * * * * * * * *");
    System.out.println("* * * * * * * *");
    System.out.println(" * * * * * * * *");
  }
}