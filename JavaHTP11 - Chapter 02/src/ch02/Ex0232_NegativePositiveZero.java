package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.32 Negative, Positive and Zero Values

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0232_NegativePositiveZero {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.32 Negative, Positive and Zero Values ***\n");

    Scanner input = new Scanner(System.in);

    int number1;
    int number2;
    int number3;
    int number4;
    int number5;

    int negatives = 0;
    int positives = 0;
    int zeros = 0;

    System.out.println("Enter the First integer: ");
    number1 = input.nextInt();

    if (number1 > 0)
      positives++;
    
    if (number1 < 0)
      negatives++;
    
    if (number1 == 0)
      zeros++;


    System.out.println("Enter the Second integer: ");
    number2 = input.nextInt();

    if (number2 > 0)
      positives++;
    
    if (number2 < 0)
      negatives++;
    
    if (number2 == 0)
      zeros++;

    System.out.println("Enter the Third integer: ");
    number3 = input.nextInt();

    if (number3 > 0)
      positives++;
    
    if (number3 < 0)
      negatives++;
    
    if (number3 == 0)
      zeros++;

    
    System.out.println("Enter the Fourth integer: ");
    number4 = input.nextInt();

    if (number4 > 0)
      positives++;
    
    if (number4 < 0)
      negatives++;
    
    if (number4 == 0)
      zeros++;

    System.out.println("Enter the Fifth integer: ");
    number5 = input.nextInt();

    if (number5 > 0)
      positives++;
    
    if (number5 < 0)
      negatives++;
    
    if (number5 == 0)
      zeros++;


    System.out.printf("\n\tNumbers: %d %d %d %d %d\n\n", 
      number1, number2, number3, number4, number5);

    System.out.printf("\tNegatives: %d\n", negatives);
    System.out.printf("\tPositives: %d\n", positives);
    System.out.printf("\tZeros: %d\n", zeros);
  }
}