package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.17 Arithmetic, Smallest and Largest

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0217_ArithmeticSmallestAndLargest {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.17 Arithmetic, Smallest and Largest ***\n");

    Scanner input = new Scanner(System.in);

    int number1;
    int number2;
    int number3;

    System.out.println("Enter the First integer: ");
    number1 = input.nextInt();

    System.out.println("Enter the Second integer: ");
    number2 = input.nextInt();

    System.out.println("Enter the Third integer: ");
    number3 = input.nextInt();

    System.out.printf("\n\tNumbers: %d %d %d\n\n", number1, number2, number3);

    System.out.printf("\tSum: %d\n", number1 + number2 + number3);
    System.out.printf("\tAverage: %d\n", (number1 + number2 + number3) / 3);
    System.out.printf("\tProduct: %d\n", number1 * number2 * number3);
    System.out.printf("\tSmallest: %d\n", 
      Math.min( Math.min(number1, number2), number3) );
    System.out.printf("\tLargest: %d\n", 
      Math.max( Math.max(number1, number2), number3) );

  }
}