package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.16 Comparing Integers

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0216_ComparingIntegers {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.16 Comparing Integers ***\n");

    Scanner input = new Scanner(System.in);

    int number;
    int numberSquare;

    System.out.println("Enter an Integer number: ");
    number = input.nextInt();
    numberSquare = number * number;

    if (number > 100) {
      System.out.printf("\t%d > %d%n", number, 100);
    }

    if (numberSquare > 100) {
      System.out.printf("\t%d > %d%n", numberSquare, 100);
    }

    if (number < 100) {
      System.out.printf("\t%d < %d%n", number, 100);
    }

    if (numberSquare < 100) {
      System.out.printf("\t%d < %d%n", numberSquare, 100);
    }

    if (number == 100) {
      System.out.printf("\t%d == %d%n", number, 100);
    }

    if (numberSquare == 100) {
      System.out.printf("\t%d == %d%n", numberSquare, 100);
    }

    if (number != 100) {
      System.out.printf("\t%d != %d%n", number, 100);
    }

    if (numberSquare != 100) {
      System.out.printf("\t%d != %d%n", numberSquare, 100);
    }
    
  }
}