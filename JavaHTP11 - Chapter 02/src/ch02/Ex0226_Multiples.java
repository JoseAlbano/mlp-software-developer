package ch02;
/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.26 Multiples

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0226_Multiples {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.26 Multiples ***\n");

    Scanner input = new Scanner(System.in);

    int number1;
    int number2;
    String isMultiple = "is Not Multiple of";

    System.out.println("Enter the First integer: ");
    number1 = input.nextInt();

    System.out.println("Enter the Second integer: ");
    number2 = input.nextInt();
    
    if ( (number1 * 3) % (number2 * 2) == 0  ) {
      isMultiple = "is Multiple of";
    }

    System.out.printf("\n\t%d by 3 %s %d by 2\n", 
      number1, isMultiple, number2);
  }
}