package ch02;
/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.8 a

/**
 * @author José Albano
 */
public class Ex0208a {

  public static void main(String[] args) {
    System.out.println("*** Exercise 2.8 ***");

    System.out.println("\ta)");

    System.out.print("Enter an integer: ");
  }
}
