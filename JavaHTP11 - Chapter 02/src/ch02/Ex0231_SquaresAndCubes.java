package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.31 Table of Squares and Cubes

/**
 * @author José Albano
 */
public class Ex0231_SquaresAndCubes {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.31 Table of Squares and Cubes ***\n");

    int number;

    System.out.printf("\t%s\t%s\t%s\n", "number", "square", "cube");

    number = 0;    
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);
    
    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);

    number++;
    System.out.printf("\t%d\t%d\t%d\n", 
      number, number * number, number * number * number);
  }
}