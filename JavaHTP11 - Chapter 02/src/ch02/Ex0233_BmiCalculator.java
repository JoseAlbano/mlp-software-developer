package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Making a Difference exercise
// Exercise 2.33 Body Mass Index Calculator

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0233_BmiCalculator {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.33 Body Mass Index Calculator ***\n");

    Scanner input = new Scanner(System.in);

    double weightInKilograms;
    double heightInMeters;
    double bmi;
    
    System.out.println("Enter the Weight in Kilograms: ");
    weightInKilograms = input.nextDouble();

    System.out.println("Enter the Height in Meters: ");
    heightInMeters = input.nextDouble();

    
    bmi = weightInKilograms / (heightInMeters * heightInMeters);
    
    
    System.out.printf("\n\tWeight (Kg): %f", weightInKilograms );
    System.out.printf("\n\tHeight (Meters): %f", heightInMeters );
    
    System.out.printf("\n\n\tBody Mass Index: %f\n", bmi );

    System.out.println("\n\tBMI Categories:\n" +
      "\n\t\tUnderweight = <18.5\n\t\tNormal weight = 18.5–24.9" + 
      "\n\t\tOverweight = 25–29.9\n\t\tObesity = BMI of 30 or greater");
  }
}