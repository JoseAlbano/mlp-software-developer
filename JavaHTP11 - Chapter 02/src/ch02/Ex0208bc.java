package ch02;
/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.8 b y c

/**
 * @author José Albano
 */
public class Ex0208bc {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.8 ***");

    System.out.println("\n\tb)");

    int a;
    int b = 7;
    int c = 3;

    a = b * c;

    System.out.printf("\t\t%d * %d = %d", b, c, a);

    
    System.out.println("\n\n\tc)");

    System.out.println("\t\tThis program performs a sample payroll calculation.");
  }
}