package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Making a Difference exercise
// Exercise 2.34 World Population Growth Calculator

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0234_WorldPopulationGrowth {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.34 World Population Growth Calculator ***\n");

    Scanner input = new Scanner(System.in);

    long worldPopulation;
    double populationGrowthRate;
    
    System.out.println("Enter the Current World Population: ");
    worldPopulation = input.nextLong();

    System.out.println("\nEnter the Annual World Population Growth Rate (%): ");
    populationGrowthRate = input.nextDouble();

    worldPopulation = worldPopulation + 
        (int) (worldPopulation * populationGrowthRate / 100);

    System.out.printf("\n\tEstimated World Population after %d year: %d", 
      1, worldPopulation );

    worldPopulation = worldPopulation + 
        (int) (worldPopulation * populationGrowthRate / 100);
    
    System.out.printf("\n\tEstimated World Population after %d year: %d", 
      2, worldPopulation );

    worldPopulation = worldPopulation + 
        (int) (worldPopulation * populationGrowthRate / 100);

    System.out.printf("\n\tEstimated World Population after %d year: %d", 
      3, worldPopulation );

    worldPopulation = worldPopulation + 
        (int) (worldPopulation * populationGrowthRate / 100);

    System.out.printf("\n\tEstimated World Population after %d year: %d", 
      4, worldPopulation );

    worldPopulation = worldPopulation + 
        (int) (worldPopulation * populationGrowthRate / 100);

    System.out.printf("\n\tEstimated World Population after %d year: %d", 
      5, worldPopulation );

    System.out.println();
  }
}