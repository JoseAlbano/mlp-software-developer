package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.24 Largest and Smallest Integers

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0224_LargestAndSmallest {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.24 Largest and Smallest Integers ***\n");

    Scanner input = new Scanner(System.in);

    int number1;
    int number2;
    int number3;
    int number4;
    int number5;
    int smallest;
    int largest;

    System.out.println("Enter the First integer: ");
    number1 = input.nextInt();

    System.out.println("Enter the Second integer: ");
    number2 = input.nextInt();

    System.out.println("Enter the Third integer: ");
    number3 = input.nextInt();

    System.out.println("Enter the Fourth integer: ");
    number4 = input.nextInt();

    System.out.println("Enter the Fifth integer: ");
    number5 = input.nextInt();

    System.out.printf("\n\tNumbers: %d %d %d %d %d\n\n", 
      number1, number2, number3, number4, number5);

    smallest = number1;
    largest = number1;

    if (number2 < smallest) {
      smallest = number2;
    }

    if (number3 < smallest) {
      smallest = number3;
    }

    if (number4 < smallest) {
      smallest = number4;
    }

    if (number5 < smallest) {
      smallest = number5;
    }


    if (number2 > largest) {
      largest = number2;
    }

    if (number3 > largest) {
      largest = number3;
    }

    if (number4 > largest) {
      largest = number4;
    }

    if (number5 > largest) {
      largest = number5;
    }


    System.out.printf("\tSmallest: %d\n", smallest);
    System.out.printf("\tLargest: %d\n", largest);
  }
}