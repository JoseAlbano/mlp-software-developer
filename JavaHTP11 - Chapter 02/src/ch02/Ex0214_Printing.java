/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.14
package ch02;

/**
 * @author José Albano
 */
public class Ex0214_Printing {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.14 ***");

    System.out.println("\nUsing one System.out.println");
    System.out.println("\t12 34");

    System.out.println("\nUsing four System.out.print");
    System.out.print("\t1");
    System.out.print("2 ");
    System.out.print("3");
    System.out.print("4");

    System.out.println("\n\nUsing one System.out.printf");
    System.out.printf("\t%d%d %d%d%n", 1, 2, 3, 4);
  }
}