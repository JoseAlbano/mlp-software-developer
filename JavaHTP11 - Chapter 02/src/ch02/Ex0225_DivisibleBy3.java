package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.25 Divisible by 3

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0225_DivisibleBy3 {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.25 Divisible by 3 ***\n");

    Scanner input = new Scanner(System.in);

    int number;
    String message;

    System.out.println("Enter the an Integer: ");
    number = input.nextInt();

    message =  "is NOT Divisible by 3";

    if (number % 3 == 0) {
      message =  "is Divisible by 3";
    }

    System.out.printf("\n\t%d %s\n", number, message);
  }
}