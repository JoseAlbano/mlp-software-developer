package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.29 Integer Value of a Character

/**
 * @author José Albano
 */
public class Ex0229_ValueOfCharacter {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.29 Integer Value of a Character ***\n");

    System.out.printf("\tThe character %c has the value %d%n", 'A', ((int) 'A'));
    System.out.printf("\tThe character %c has the value %d%n", 'B', ((int) 'B'));
    System.out.printf("\tThe character %c has the value %d%n", 'C', ((int) 'C'));
    
    System.out.printf("\tThe character %c has the value %d%n", 'a', ((int) 'a'));
    System.out.printf("\tThe character %c has the value %d%n", 'b', ((int) 'b'));
    System.out.printf("\tThe character %c has the value %d%n", 'c', ((int) 'c'));
    
    System.out.printf("\tThe character %c has the value %d%n", '0', ((int) '0'));
    System.out.printf("\tThe character %c has the value %d%n", '1', ((int) '1'));
    System.out.printf("\tThe character %c has the value %d%n", '2', ((int) '2'));
    
    System.out.printf("\tThe character %c has the value %d%n", '$', ((int) '$'));
    System.out.printf("\tThe character %c has the value %d%n", '*', ((int) '*'));
    System.out.printf("\tThe character %c has the value %d%n", '+', ((int) '+'));
    System.out.printf("\tThe character %c has the value %d%n", '/', ((int) '/'));
    
    System.out.printf("\tThe blank character has the value %d%n", ((int) ' '));
  }
}