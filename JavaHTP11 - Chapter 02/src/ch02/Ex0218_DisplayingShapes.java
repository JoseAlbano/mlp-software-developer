package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.18 Displaying Shapes with Asterisks

/**
 * @author José Albano
 */
public class Ex0218_DisplayingShapes {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.18 Displaying Shapes with Asterisks ***\n");

    System.out.println("\t*********     ***       *        *");
    System.out.println("\t*       *   *     *    ***      * *");
    System.out.println("\t*       *  *       *  *****    *   *");
    System.out.println("\t*       *  *       *    *     *     *");
    System.out.println("\t*       *  *       *    *    *       *");
    System.out.println("\t*       *  *       *    *     *     *");
    System.out.println("\t*       *  *       *    *      *   *");
    System.out.println("\t*       *   *     *     *       * *");
    System.out.println("\t*********     ***       *        *");
    
  }
}