package ch02;

/* Chapter 2: Introduction to Java Applications; Input/Output and Operators */
// Exercise 2.15 Arithmetic

/**
 * @author José Albano
 */
import java.util.Scanner;

public class Ex0215_Arithmetic {
  public static void main(String[] args) {
    System.out.println("*** Exercise 2.15 Arithmetic ***\n");

    Scanner input = new Scanner(System.in);

    int number1;
    int number2;

    System.out.println("Enter the First integer: ");
    number1 = input.nextInt();

    System.out.println("Enter the Second integer: ");
    number2 = input.nextInt();

    System.out.printf("\n\tThe Square of %d is %d.\n", 
      number1, number1 * number1);
    System.out.printf("\tThe Square of %d is %d.\n", 
      number2, number2 * number2);

    System.out.printf("\tThe Sum of the Squares of %d and %d is %d.\n", 
      number1, number2, number1 * number1 + number2 * number2);

    System.out.printf("\tThe Difference of the Squares of %d and %d is %d.\n", 
      number1, number2, number1 * number1 - number2 * number2);
  }
}